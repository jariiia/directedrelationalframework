from interval import interval

# Domain constants

LIMIT = 1

valuation_domain = interval[-LIMIT, LIMIT]
acceptance_domain = interval[0, LIMIT]

# DRF visualisation constants

LABEL_FONT_SIZE = 15
NODE_FONT_SIZE = 15
NODE_SIZE = 600
NUMBER_OF_DECIMALS = 3
ROOT_COLOUR = 'g'
OTHER_COLOUR = 'r'
