"""Provides RelationalFramework, DRF, Opinion, AggregationFunctions, classes for collective decision making.
   Mathematical details about these classes can be found at .....
"""
import random
import networkx as nx
import matplotlib.pyplot as plt
# Uncomment to run profiling
# import datetime as dt
from halp.directed_hypergraph import DirectedHypergraph
import numpy as np

__author__ = "Jordi Ganzer, Juan A. Rodríguez-Aguilar, Natalia Criado, Maite López-Sánchez, and Simon Parsons"
__copyright__ = "Copyright 2019"
__credits__ = ["Jordi Ganzer", "Juan A. Rodríguez-Aguilar", "Natalia Criado",
                    "Maite López-Sánchez", "Simon Parsons"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Juan A. Rodríguez-Aguilar"
__email__ = "jar@iiia.csic.es"
__status__ = "Development"

class DRF:
    """
        A class used to represent and access a Relational Framework

        The framework is represented as a directed acyclic graph using the DiGraph networkx object.

        Attributes
        ----------
        sentences : list
            a list of the sentences composing the framework
        connections : list
            a list of pairwise connections between sentences

        Methods
        -------
        gets_sentences()
            Returns the sentences in the framework
        get_connections()
            Returns the connections in the framework
        get_graph()
            Returns the networkx directed graph object encoding the framework
        set_connection(source,target)
            Sets a directed connection (edge) from a source sentence to a target sentence
        set_connections(list_of_connections)
            Sets directed connections (edges) from a list of connections
        get_descendants(sentence)
            Returns the sentences to which a given sentence is connected to
        has_descendants(sentence)
            Checks whether a sentence is connected to any other sentence in the framework
        topological_sorting()
            Returns the topological sorting of the directed acyclic graph as a list
        draw()
            Draws the directed acyclic graph representing the relational framework
        """

    def __init__(self, sentences=None, hyperedges=None, targets=None):
        self.relationships = DirectedHypergraph()
        if sentences:
            self.sentences = sentences
            self.relationships.add_nodes(self.sentences)
        if hyperedges:
            self.relationships.add_hyperedges(hyperedges)
        if targets:
            self.targets = targets
        self.descendants = {}

    def get_sentences(self):
        return self.sentences

    def get_relationships(self):
        return self.relationships.get_hyperedge_id_set()

    def get_hypergraph(self):
        return self.relationships

    def get_relationship_id(self,tail,head):
        return self.relationships.get_hyperedge_id(tail,head)

    def get_relationship_head(self,relationship_id):
        return self.relationships.get_hyperedge_head(relationship_id)[0]

    def get_relationship_weight(self,relationship_id):
        return self.relationships.get_hyperedge_weight(relationship_id)

    def get_number_of_relationships(self):
        return self.relationships.get_hyperedge_id_set().__len__()

    def get_number_of_sentences(self):
        return len(self.sentences)

    def get_targets(self):
        return self.targets

    def set_sentences(self,sentences):
        self.sentences = sentences
        self.relationships.add_nodes(sentences)

    def set_targets(self, targets):
        for sentence in targets:
            if any(True for _ in self.relationships.get_predecessors([sentence])):
                print(self.relationships.get_predecessors([sentence]))
                raise Exception('Some targets have ancestors')
        self.targets = targets

    def set_relationship(self, tail, head):
        if not self.relationships.has_hyperedge(tail,head):
            self.relationships.add_hyperedge(tail, head)

    def set_relationships(self, hyperedges):
        self.relationships.add_hyperedges(hyperedges)

    def hash_descendants(self):
        self.descendants = dict((s, []) for s in self.sentences)
        relationships = self.get_relationships()
        for r in relationships:
            tail = self.get_hypergraph().get_hyperedge_tail(r)
            for node in tail:
                self.descendants[node].append(r)

    def get_descendants(self, sentence):
        hyperedges = self.descendants[sentence]
        return [self.relationships.get_hyperedge_head(h)[0] for h in hyperedges]

    def get_descending_relationships(self,sentence):
        return self.descendants[sentence]

    def load_dag(self,dag):
        self.set_sentences(dag.nodes)
        relationships = [([e[0]],[e[1]]) for e in dag.edges]
        self.set_relationships(relationships)

    def print_hypergraph(self):
        l = self.relationships.get_hyperedge_id_set()
        print('********************** HYPERGRAPH **********************')
        print("Edges: ",l)
        for e in l:
            tail = self.relationships.get_hyperedge_tail(e)
            head = self.relationships.get_hyperedge_head(e)
            print(e,"--> Tail: ", tail,";",\
                  "Head: ", head)
            print("Hyperedge id: ",self.get_hypergraph().get_hyperedge_id(tail,head))
        print('********************************************************\n')

    def is_B_hypergraph(self):
        return self.relationships.is_B_hypergraph()

    def to_DAG(self):
        dag = nx.DiGraph()
        dag.add_nodes_from(self.sentences)
        hyperedges = self.relationships.get_hyperedge_id_set()
        for e in hyperedges:
            tail = self.relationships.get_hyperedge_tail(e)
            head = self.relationships.get_hyperedge_head(e)
            for t in tail:
                dag.add_edge(t,head[0])
        return dag

    def draw_DAG(self):
        dag = self.to_DAG()
        plt.figure()
        pos = nx.spring_layout(dag)
        nx.draw_networkx_nodes(dag, pos)
        nx.draw_networkx_edges(dag, pos)
        labels = {}
        for sentence in self.sentences:
            labels[sentence] = str(sentence)
        nx.draw_networkx_labels(dag, pos, labels)
        plt.title('Relational Framework')
        plt.axis('off')
        plt.show()

    def topological_sorting(self):
        return list(nx.topological_sort(self.to_DAG()))

    def write(self,file_name):
        self.relationships.write(file_name)



class Opinion:
    """
         A class used to encode and opinion over a Directed Relational Framework (DRF)

         An opinion assigns a valuation per sentence and an acceptance degree per connection between sentences.

         Attributes
         ----------
         drf : DRF
             the DRF over which the opinion is made
         acceptance_domain: interval
             range of values that acceptance values over relationships between sentences can take on
         valuation_domain: interval
             range of values that valuations over sentences can take on
         acceptance : dict
             a dictionary that contains the acceptance value per relationship between sentences, pair of sentences
        valuation: dict
            a dictionary that contains the valuation per sentence

         Methods
         -------
         set_acceptance(connection,degree)
            Sets the acceptance value of the given connection to degree
         set_acceptance_all(acceptances)
            Sets all acceptance values using the given acceptances dictionary
         set_valuation(sentence,value)
            Sets the valuation of the given sentence to value
         set_valuation_all(valuations)
            Sets all valuations using the given valuations dictionary
         get_acceptance(connection)
            Returns the acceptance value of the given connection
         get_acceptance_all()
            Returns all acceptance values over relationships between sentences as a dictionary
         get_valuation(sentence)
            Returns the valuation of the given sentence
         get_valuation_all(valuations)
            Returns all valuations over sentences as a dictionary
         get_estimated_value(sentence)
             Computes the estimated value of the sentence
         is_sentence_e_coherent(sentence, epsilon)
             Returns whether the sentence is epsilon-coherent
         is_e_coherent(sentence, epsilon)
             Computes whether the whole opinion is epsilon-coherent
         scale(alpha):
             Scales all valuations and acceptances by alpha
         add_opinion(opinion):
             Adds an opinion to this object and returns a new Opinion object with the result
         draw()
             Draws the opinion over the drf
         """
    def __init__(self, drf, acceptance_domain, valuation_domain):
        self.drf = drf
        self.acceptance_domain = acceptance_domain
        self.valuation_domain = valuation_domain
        self.acceptance = {}
        self.valuation = {}
        for r in self.drf.get_relationships():
            self.acceptance[r] = [acceptance_domain.midpoint[0].inf]
        for s in self.drf.get_sentences():
            self.valuation[s] = valuation_domain.midpoint[0].inf

    def get_acceptance_by_id(self, relationship_id):
        return self.acceptance[relationship_id]

    def get_acceptance(self, tail, head):
        r = self.drf.get_relationship_id(tail,head)
        return self.acceptance[r]

    def get_acceptance_all(self):
        return self.acceptance

    def set_acceptance(self, tail,head, degree):
        r = self.drf.get_relationship_id(tail,head)
        if self.drf.get_relationship_weight(r) != len(degree):
            raise Exception('Error: Mismatch between cardinality of hyperedge an number of acceptance values')
        self.acceptance[r] = degree

    def set_acceptance_by_id(self,relationship_id, degree):
        self.acceptance[relationship_id] = degree

    def set_acceptance_all(self, acceptances):
        self.acceptance = acceptances

    def set_valuation(self, sentence, value):
        self.valuation[sentence] = value

    def get_valuation(self, sentence):
        return self.valuation[sentence]

    def get_valuation_all(self):
        return self.valuation

    def set_valuation_all(self, valuations):
        self.valuation = valuations

    def get_estimated_value(self, sentence):
        estimated_value = 0.0
        total_acceptance = 0.0
        descending_relationships = self.drf.get_descending_relationships(sentence)
        for relationship in descending_relationships:
            descendant = self.drf.get_relationship_head(relationship)
            acceptances = self.acceptance[relationship]
            for acceptance in acceptances:
                estimated_value += self.valuation[descendant] * acceptance
                total_acceptance += acceptance
        if descending_relationships:
            if total_acceptance == 0:
                estimated_value = 0
            else:
                estimated_value = estimated_value / total_acceptance
        else:
            estimated_value = self.get_valuation(sentence)
        return total_acceptance, estimated_value

    def is_sentence_e_coherent(self, sentence, epsilon):
        total_acceptance, estimated_value = self.get_estimated_value(sentence)
        if total_acceptance == 0:
            return True
        else:
            return abs(self.valuation[sentence] - estimated_value) <= epsilon

    def is_e_coherent(self, epsilon):
        is_coherent = True
        for key, value in self.valuation.items():
            if not self.is_sentence_e_coherent(key, epsilon):
                is_coherent = False
                break
        return is_coherent

    def scale(self, alpha):
        for r in self.drf.get_relationships():
            acceptances = self.acceptance[r]
            self.acceptance[r] = [a * alpha for a in acceptances]
        for s in self.drf.get_sentences():
            self.valuation[s] *= alpha

    def add_opinion(self, opinion):
        total_acceptance = {}
        total_valuation = {}
        total_opinion = Opinion(self.drf, self.acceptance_domain, self.valuation_domain)
        for sentence in self.drf.get_sentences():
            total_valuation[sentence] = self.valuation[sentence] + opinion.valuation[sentence]
        for relationship in self.drf.get_relationships():
            total_acceptance[relationship] = \
                [x + y for x, y in zip(self.acceptance[relationship],opinion.acceptance[relationship])]
        total_opinion.set_valuation_all(total_valuation)
        total_opinion.set_acceptance_all(total_acceptance)
        return total_opinion

    def print(self):
        print("Valuations:\n")
        for sentence in self.drf.get_sentences():
            print(sentence,": ",self.get_valuation(sentence),"\n")
        print("Acceptances:\n")
        for relationship in self.drf.get_relationships():
            print(relationship,": ",self.get_acceptance_by_id(relationship),"\n")
        print("Estimated valuations:\n")
        for sentence in self.drf.get_sentences():
            print(sentence, ": ", (self.get_estimated_value(sentence))[1], "\n")

class Profile:

    def __init__(self, drf,acceptance_domain,valuation_domain,random_mode=False,number_of_opinions=0):
        self.drf = drf
        self.opinions = []
        if not random_mode:
            self.aggregated_acceptances = dict.fromkeys(self.drf.get_relationships(), [])
            self.aggregated_valuations = dict.fromkeys(self.drf.get_sentences(), 0.0)
            self.acceptance_domain = acceptance_domain
            self.valuation_domain = valuation_domain
        else:
            number_of_sentences = self.drf.get_number_of_sentences()
            number_of_relationships = self.drf.get_number_of_relationships()
            valuations = np.random.uniform(low=-1.0, high=1.0, size=(number_of_opinions,number_of_sentences))
            acceptances = np.random.rand(number_of_opinions,number_of_relationships)
            added_valuations = np.sum(valuations,axis=0)
            added_acceptances = np.sum(acceptances,axis=0)
            self.aggregated_valuations = dict.fromkeys(self.drf.get_sentences(), added_valuations)
            self.aggregated_acceptances = dict.fromkeys(self.drf.get_relationships(), added_acceptances)

    def size(self):
        return len(self.opinions)

    def add_opinion(self,opinion):
        self.opinions.append(opinion)
        relationships = self.drf.get_relationships()
        for relationship in relationships:
            if self.aggregated_acceptances[relationship]:
                self.aggregated_acceptances[relationship] = \
                    [x + y for x, y in zip( self.aggregated_acceptances[relationship], opinion.get_acceptance_by_id(relationship))]
            else:
                self.aggregated_acceptances[relationship] = opinion.get_acceptance_by_id(relationship)
        for sentence in self.drf.get_sentences():
            self.aggregated_valuations[sentence] += opinion.get_valuation(sentence)

    def remove_opinion(self,opinion):
        self.opinions.remove(opinion)
        relationships = self.drf.get_relationships()
        for relationship in relationships:
            self.aggregated_acceptances[relationship]= \
                [x - y for x, y in zip(self.aggregated_acceptances[relationship], opinion.get_acceptance_by_id(relationship))]
        for sentence in self.drf.get_sentences():
            self.aggregated_valuations[sentence] -= opinion.get_valuation(sentence)

    def get_opinions(self):
        return self.opinions

    def get_aggregated_acceptances(self):
        aggregated = dict(self.aggregated_acceptances)
        for k,v in aggregated.items():
            aggregated[k] = [acceptance/self.size() for acceptance in v]
        return aggregated

    def get_aggregated_valuations(self):
        return {k: v / self.size() for k, v in self.aggregated_valuations.items()}

    def generate_random_profile(self, number_of_opinions):
        for i in range(number_of_opinions):
            opinion = Opinion(self.drf, self.acceptance_domain, self.valuation_domain)
            for sentence in self.drf.get_sentences():
                new_valuation = random.uniform(-1, 1)
                opinion.set_valuation(sentence, new_valuation)
                self.aggregated_valuations[sentence] += new_valuation
            for relationship in self.drf.get_relationships():
                new_acceptance = random.uniform(0, 1)
                opinion.set_acceptance_by_id(relationship, new_acceptance)
                self.aggregated_acceptances[relationship] += new_acceptance
            self.add_opinion(opinion)

class RandomProfile:

    def __init__(self, drf,number_of_opinions):
        self.drf = drf
        self.number_of_opinions = number_of_opinions
        number_of_sentences = self.drf.get_number_of_sentences()
        number_of_relationships = self.drf.get_number_of_relationships()
        self.valuations = np.random.uniform(low=-1.0, high=1.0, size=(number_of_opinions,number_of_sentences))
        self.acceptances = np.random.rand(number_of_opinions,number_of_relationships)

    def get_aggregated_acceptances(self):
        # begin = dt.datetime.now()
        added_acceptances = np.sum(self.acceptances,axis=0)
        # self.aggregated_acceptances = dict(zip(self.drf.get_relationships(), added_acceptances.T))
        self.aggregated_acceptances = dict(zip(self.drf.get_relationships(), added_acceptances.T / self.number_of_opinions))
        # end = dt.datetime.now()
        # acceptance_time = (end - begin).total_seconds()
        # print('Building aggregated acceptances time: ',acceptance_time)
        return self.aggregated_acceptances

    def get_aggregated_valuations(self):
        # begin = dt.datetime.now()
        added_valuations = np.sum(self.valuations,axis=0)
        # end = dt.datetime.now()
        # valuation_time = (end - begin).total_seconds()
        # print('Building aggregated valuations time: ', valuation_time)
        self.aggregated_valuations = dict(zip(self.drf.get_sentences(),added_valuations.T / self.number_of_opinions))
        return self.aggregated_valuations

    def size(self):
        return self.number_of_opinions


class AggregationFunctions:
    """
         A class used to encode and opinion over a Directed Relational Framework (DRF)

         An opinion assigns a valuation per sentence and an acceptance degree per connection between sentences.

         Attributes
         ----------
         drf : DRF
             the DRF over which the aggregation of opinions is made
         acceptance_domain: interval
             range of values that acceptance values over relationships between sentences can take on
         valuation_domain: interval
             range of values that valuations over sentences can take on
         profile : list
             list of Opinions over the drf
        number_of_opinions: integer
            number of opinions in the profile

         Methods
         -------
         average_sentence_valuation(sentence)
            Computes the average of the valuations in the profile over a sentence
         average_connection_acceptance(connection)
            Computes the average of the acceptances in the profile over a connection
         average_estimated_valuation(sentence)
            Computes the average estimated valuation over a sentence based on the average valuations over
            the sentence of the opinions in the profile
         direct_function_all()
            Computes the collective opinion after aggregating the opinions in the profile using the Direct Function
         indirect_function_all()
            Computes the collective opinion after aggregating the opinions in the profile using the Indirect Function
         balanced_function_all()
            Computes the collective opinion after aggregating the opinions in the profile using the Balanced Function,
            which combines the Opinions resulting from the Direct and Indirect aggregation functions
         recursive_function_all()
            Computes the collective opinion after aggregating the opinions in the profile using the Recursive Function
         generalised_recursive_function_all()
            Computes the collective opinion after aggregating the opinions in the profile using the Generalised
            Recursive Function, which combines de Direct Function and the Recursive Function
         """
    def __init__(self, drf, profile, acceptance_domain, valuation_domain):
        self.drf = drf
        self.profile = profile
        self.acceptance_domain = acceptance_domain
        self.valuation_domain = valuation_domain
        self.number_of_opinions = profile.size()

    def average_sentence_valuation(self, sentence):
        total_valuation = 0.0
        for opinion in self.profile:
            total_valuation += opinion.get_valuation(sentence)
        return total_valuation / self.number_of_opinions

    def average_estimated_valuation(self, sentence):
        total_estimated_valuation = 0.0
        for opinion in self.profile.get_opinions():
            total_estimated_valuation += opinion.get_estimated_value(sentence)[1]
        return total_estimated_valuation / self.number_of_opinions

    def direct_function(self):
        aggregated_valuations = self.profile.get_aggregated_valuations()
        aggregated_acceptances = self.profile.get_aggregated_acceptances()
        collective_opinion = Opinion(self.drf, self.acceptance_domain, self.valuation_domain)
        collective_opinion.set_acceptance_all(aggregated_acceptances)
        collective_opinion.set_valuation_all(aggregated_valuations)
        return collective_opinion

    def indirect_function(self):
        aggregated_valuations = {}
        for sentence in self.drf.get_sentences():
            individual_valuation = self.average_estimated_valuation(sentence)
            aggregated_valuations[sentence] = individual_valuation
        aggregated_acceptances = self.profile.get_aggregated_acceptances()
        collective_opinion = Opinion(self.drf, self.acceptance_domain, self.valuation_domain)
        collective_opinion.set_acceptance_all(aggregated_acceptances)
        collective_opinion.set_valuation_all(aggregated_valuations)
        return collective_opinion

    def balanced_function(self, alpha=0.5):
        direct_opinion = self.direct_function()
        indirect_opinion = self.indirect_function()
        direct_opinion.scale(alpha)
        indirect_opinion.scale(1.0 - alpha)
        return direct_opinion.add_opinion(indirect_opinion)

    def recursive_function(self):
        # In order to compute aggregated valuations at once, we can do a reversed topological sorting
        # and then compute aggregated valuations
        #begin = dt.datetime.now()
        sorted_sentences = list(reversed(self.drf.topological_sorting()))
        #end = dt.datetime.now()
        #topological_time = (end - begin).total_seconds()
        #print('Topological sorting time: ',topological_time)
        # Compute averaged acceptance values at once
        aggregated_acceptances = self.profile.get_aggregated_acceptances()
        # Get aggregated valuations from profile
        aggregated_valuations = self.profile.get_aggregated_valuations()
        # Compute aggregated valuations
        #begin = dt.datetime.now()
        for sentence in sorted_sentences:
            individual_valuation = 0.0
            aggregated_acceptance = 0.0
            descending_relationships = self.drf.get_descending_relationships(sentence)
            if descending_relationships:
                for relationship in descending_relationships:
                    descendant = self.drf.get_relationship_head(relationship)
                    acceptance = aggregated_acceptances[relationship]
                    if isinstance(acceptance,list):
                        for a in acceptance:
                            individual_valuation += aggregated_valuations[descendant] * a
                            aggregated_acceptance += a
                    else:
                        individual_valuation += aggregated_valuations[descendant] * acceptance
                        aggregated_acceptance += acceptance
                aggregated_valuations[sentence] = individual_valuation / aggregated_acceptance
        #end = dt.datetime.now()
        #valuation_time = (end - begin).total_seconds()
        #print('Valuation computation time: ', valuation_time)
        collective_opinion = Opinion(self.drf, self.acceptance_domain, self.valuation_domain)
        collective_opinion.set_acceptance_all(aggregated_acceptances)
        collective_opinion.set_valuation_all(aggregated_valuations)
        return collective_opinion

    def generalised_recursive_function(self, alpha):
        collective_opinion_recursive = self.recursive_function()
        collective_opinion_direct = self.direct_function()
        collective_opinion_direct.scale(alpha)
        collective_opinion_recursive.scale(1.0 - alpha)
        return collective_opinion_direct.add_opinion(collective_opinion_recursive)


