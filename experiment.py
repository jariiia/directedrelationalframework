from RelationalModel import *
from generator import *
import matplotlib.pyplot as plt
import datetime as dt
import numpy as np
import warnings
from matplotlib.cbook.deprecation import MatplotlibDeprecationWarning
# uncomment to run memory profile
# from memory_profiler import profile
import configparser
import csv
from pathlib import Path

__author__ = "Jordi Ganzer, Juan A. Rodríguez-Aguilar, Natalia Criado, Maite López-Sánchez, and Simon Parsons"
__copyright__ = "Copyright 2019"
__credits__ = ["Jordi Ganzer", "Juan A. Rodríguez-Aguilar", "Natalia Criado",
                    "Maite López-Sánchez", "Simon Parsons"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Juan A. Rodríguez-Aguilar"
__email__ = "jar@iiia.csic.es"
__status__ = "Development"

# uncomment to run memory profile
# @profile
def generate_and_solve(number_of_problems, number_of_sentences, number_of_opinions, average_out_degree,hyperedge_tail_size=1):
    solved_problems = 0
    solving_times =[]
    while solved_problems < number_of_problems:
        out_degree = average_out_degree / number_of_sentences
        drf = generate_random_hypergraph(number_of_sentences,out_degree,hyperedge_tail_size)
        print('Number of sentences: ',drf.get_number_of_sentences())
        print('Number of relationships: ',drf.get_number_of_relationships())
        if drf:
            begin = dt.datetime.now()
            profile = RandomProfile(drf,number_of_opinions)
            end = dt.datetime.now()
            generation_time = (end - begin).total_seconds()
            print('Profile generation time: ',generation_time)
            f = AggregationFunctions(drf, profile, acceptance_domain, valuation_domain)
            begin = dt.datetime.now()
            f.recursive_function()
            end = dt.datetime.now()
            solving_time = (end - begin).total_seconds()
            solved_problems += 1
            solving_times.append(solving_time)
            # The following lines are important to clean memory
            profile = None
            del profile
            drf = None
            del drf
            f = None
            del f
    return solving_times

def density_analysis(number_of_problems,number_of_participants,number_of_sentences,connectivity,results_file,hyperedge_tail_size=1):
    print('Computing sensitivity to density of the directed relational framework...')
    print('\n------------------------------\nNumber of participants: ', number_of_participants)
    results = []
    for c in connectivity:
        print('Average out degree: ',c)
        for i in number_of_sentences:
            solving_times = generate_and_solve(number_of_problems, i, number_of_participants, c)
            print('\n********** Solving time:', np.mean(solving_times), '+-', 2.0 * np.std(solving_times))
            results.append([c,i,np.mean(solving_times),2.0 * np.std(solving_times)])
    with open(results_file, mode='w') as density_results_file:
        density_writer = csv.writer(density_results_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        density_writer.writerow(['connectivity', 'sentences', 'time','error'])
        for result in results:
            density_writer.writerow(result)

def draw_density_analysis(results_file,output_file):
    fig, ax = plt.subplots()
    set_of_sentences = set()
    low_times,low_errors,medium_times,medium_errors,high_times,high_errors = ([] for i in range(6))
    with open(results_file, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            connectivity = float(row["connectivity"])
            sentences = int(row["sentences"])
            set_of_sentences.add(sentences)
            solving_time = float(row["time"])
            error = float(row["error"])
            if connectivity == 2.0:
                low_times.append(solving_time)
                low_errors.append(error)
            if connectivity == 5.0:
                medium_times.append(solving_time)
                medium_errors.append(error)
            if connectivity == 10.0:
                high_times.append(solving_time)
                high_errors.append(error)
    number_of_sentences  = list(set_of_sentences)
    number_of_sentences.sort()
    ax.plot(number_of_sentences, low_times, 'r-', label='Low density')
    ax.errorbar(number_of_sentences, low_times, yerr=low_errors,fmt='ro')
    ax.plot(number_of_sentences, medium_times, 'bs--', label='Medium density')
    ax.errorbar(number_of_sentences, medium_times, yerr=medium_errors,fmt='bo')
    ax.plot(number_of_sentences, high_times, 'g^:', label='High density')
    ax.errorbar(number_of_sentences, high_times, yerr=high_errors,fmt='go')
    plt.xlabel('Number of sentences')
    plt.ylabel('Time (s)')
    plt.title('Sensitivity to density')

    legend = ax.legend(loc='upper left', shadow=True)

    frame = legend.get_frame()
    frame.set_facecolor('0.90')

    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width

    plt.savefig(output_file,bbox_inches='tight')

def participant_analysis(number_of_problems,number_of_participants,number_of_sentences,connectivity,results_file,hyperedge_tail_size=1):
    print('Computing sensitivity to number of participants...')
    results = []
    for participants in number_of_participants:
        print('\n------------------------------\nNumber of participants: ',participants)
        for i in number_of_sentences:
            solving_times = generate_and_solve(number_of_problems, i, participants, connectivity,hyperedge_tail_size)
            print('\n********** Solving time:', np.mean(solving_times), '+-', 2.0 * np.std(solving_times),'\n')
            results.append([participants,i,np.mean(solving_times),2.0 * np.std(solving_times)])
        with open(results_file, mode='w') as participants_results_file:
            participant_writer = csv.writer(participants_results_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            participant_writer.writerow(['participants', 'sentences', 'time', 'error'])
            for result in results:
                participant_writer.writerow(result)

def draw_participant_analysis(results_file,output_file):
    fig, ax = plt.subplots()
    set_of_sentences = set()
    low_times,low_errors,medium_times,medium_errors,high_times,high_errors = ([] for i in range(6))
    with open(results_file, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            participants = int(row["participants"])
            sentences = int(row["sentences"])
            set_of_sentences.add(sentences)
            solving_time = float(row["time"])
            error = float(row["error"])
            if participants == 1000000:
                low_times.append(solving_time)
                low_errors.append(error)
            if participants == 30000000:
                medium_times.append(solving_time)
                medium_errors.append(error)
            if participants == 50000000:
                high_times.append(solving_time)
                high_errors.append(error)
    number_of_sentences = list(set_of_sentences)
    number_of_sentences.sort()
    ax.plot(number_of_sentences, low_times, 'g^:', label='$10^6$ opinions')
    ax.errorbar(number_of_sentences, low_times, yerr=low_errors, fmt='go')
    ax.plot(number_of_sentences, medium_times, 'bs--', label='$3 \cdot 10^6$ opinions')
    ax.errorbar(number_of_sentences, medium_times, yerr=medium_errors, fmt='bo')
    ax.plot(number_of_sentences, high_times, 'r-', label='$5 \cdot 10^6$ opinions')
    ax.errorbar(number_of_sentences, high_times, yerr=high_errors, fmt='ro')
    plt.xlabel('Number of sentences')
    plt.ylabel('Time (s)')
    plt.title('Sensitivity to number of participants')

    legend = ax.legend(loc='upper left', shadow=True)

    frame = legend.get_frame()
    frame.set_facecolor('0.90')

    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width

    plt.savefig(output_file,bbox_inches='tight')

def hyperedge_tail_size_analysis(number_of_problems,number_of_participants,number_of_sentences,average_out_degree,tail_sizes,results_file):
    print('Computing sensitivity to hyperedge tail size...')
    results = []
    for tail_size in tail_sizes:
        print('\n------------------------------\nNumber of participants: ', number_of_participants)
        for i in number_of_sentences:
            solving_times = generate_and_solve(number_of_problems, i, number_of_participants, average_out_degree,tail_size)
            print('\n********** Solving time:', np.mean(solving_times), '+-', 2.0 * np.std(solving_times),'\n')
            results.append([tail_size,i,np.mean(solving_times),2.0 * np.std(solving_times)])
    with open(results_file, mode='w') as relationships_results_file:
        relationships_writer = csv.writer(relationships_results_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        relationships_writer.writerow(['tail_size', 'sentences', 'time','error'])
        for result in results:
            relationships_writer.writerow(result)

def draw_hyperedge_tail_size_analysis(results_file,output_file):
    fig, ax = plt.subplots()
    set_of_sentences = set()
    low_times,low_errors,medium_times,medium_errors,high_times,high_errors = ([] for i in range(6))
    with open(results_file, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            tail_size = int(row["tail_size"])
            sentences = int(row["sentences"])
            set_of_sentences.add(sentences)
            solving_time = float(row["time"])
            error = float(row["error"])
            if tail_size == 1:
                low_times.append(solving_time)
                low_errors.append(error)
            if tail_size == 2:
                medium_times.append(solving_time)
                medium_errors.append(error)
            if tail_size == 3:
                high_times.append(solving_time)
                high_errors.append(error)
    number_of_sentences = list(set_of_sentences)
    number_of_sentences.sort()
    ax.plot(number_of_sentences, low_times, 'r-', label='r-size = 1')
    ax.errorbar(number_of_sentences, low_times, yerr=low_errors,fmt='ro')
    ax.plot(number_of_sentences, medium_times, 'bs--', label='r-size = 2')
    ax.errorbar(number_of_sentences, medium_times, yerr=medium_errors,fmt='bo')
    ax.plot(number_of_sentences, high_times, 'g^:', label='r-size = 3')
    ax.errorbar(number_of_sentences, high_times, yerr=high_errors,fmt='go')
    plt.xlabel('Number of sentences')
    plt.ylabel('Time (s)')
    plt.title('Sensitivity to relationships size')

    legend = ax.legend(loc='upper left', shadow=True)

    frame = legend.get_frame()
    frame.set_facecolor('0.90')

    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width

    plt.savefig(output_file,bbox_inches='tight')


def run_analysis(config_file):
    # Read configuration file
    config = configparser.ConfigParser()
    config.read(config_file)
    number_of_problems = config['general'].getint('number_of_problems')
    participant_analysis_on = config['general'].getboolean('participant_analysis')
    density_analysis_on = config['general'].getboolean('density_analysis')
    relationship_analysis_on = config['general'].getboolean('relationship_size_analysis')
    # Run sensitivity analysis to number of participants
    if participant_analysis_on:
        number_of_sentences = [int(s) for s in (config['participants']['number_of_sentences']).split(',')]
        average_out_degree = config['participants'].getfloat('average_out_degree')
        number_of_participants = [int(p) for p in (config['participants']['number_of_participants']).split(',')]
        participants_results_file = config['participants']['log_file']
        participant_analysis(number_of_problems, number_of_participants, number_of_sentences, average_out_degree,
                             participants_results_file)
    # Run sensitivity analysis to density of relationships
    if density_analysis_on:
        number_of_participants = config['density'].getint('number_of_participants')
        number_of_sentences = [int(s) for s in (config['density']['number_of_sentences']).split(',')]
        average_out_degree = [float(d) for d in (config['density']['average_out_degree']).split(',')]
        density_results_file = config['density']['log_file']
        density_analysis(number_of_problems, number_of_participants, number_of_sentences, average_out_degree, density_results_file)

    # Run sensitivity analysis to the size of relationships (tails of hyperedges)
    if relationship_analysis_on:
        number_of_participants = config['relationships'].getint('number_of_participants')
        number_of_sentences = [int(s) for s in (config['relationships']['number_of_sentences']).split(',')]
        average_out_degree = config['relationships'].getfloat('average_out_degree')
        tail_sizes = [int(s) for s in (config['relationships']['tail_sizes']).split(',')]
        relationships_results_file = config['relationships']['log_file']
        hyperedge_tail_size_analysis(number_of_problems,number_of_participants,number_of_sentences,average_out_degree,tail_sizes,relationships_results_file)

def plot_results(config_file):
    # Read configuration file
    config = configparser.ConfigParser()
    config.read(config_file)
    generate_graphs = config['general'].getboolean('generate_graphs')
    # Generate graphs
    if generate_graphs:
        # Plot sensitivity analysis to number of participants
        participants_results_file = config['participants']['log_file']
        participant_output_file = config['participants']['output_file']
        participants_file = Path(participants_results_file)
        if participants_file.is_file():
            print('Generating graphs for: ', participants_results_file)
            draw_participant_analysis(participants_results_file, participant_output_file)
        # Plot sensitivity analysis to density of relationships
        density_results_file = config['density']['log_file']
        density_output_file = config['density']['output_file']
        density_file = Path(density_results_file)
        if density_file.is_file():
            print('Generating graphs for: ', density_results_file)
            draw_density_analysis(density_results_file, density_output_file)
        # Plot sensitivity analysis to the size of relationships (tails of hyperedges)
        relationships_results_file = config['relationships']['log_file']
        relationships_output_file = config['relationships']['output_file']
        relationships_file = Path(relationships_results_file)
        if relationships_file.is_file():
            print('Generating graphs for: ', relationships_results_file)
            draw_hyperedge_tail_size_analysis(relationships_results_file, relationships_output_file)

if __name__ == "__main__":

    # Ignore warnings issued by networkx when plotting
    warnings.filterwarnings("ignore", category=MatplotlibDeprecationWarning)

    config_file = 'experiment.cfg'
    run_analysis(config_file)
    plot_results(config_file)

