import networkx as nx
import matplotlib.pyplot as plt
from RelationalModel import *
from statistics import mean

__author__ = "Jordi Ganzer, Juan A. Rodríguez-Aguilar, Natalia Criado, Maite López-Sánchez, and Simon Parsons"
__copyright__ = "Copyright 2019"
__credits__ = ["Jordi Ganzer", "Juan A. Rodríguez-Aguilar", "Natalia Criado",
                    "Maite López-Sánchez", "Simon Parsons"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Juan A. Rodríguez-Aguilar"
__email__ = "jar@iiia.csic.es"
__status__ = "Development"

def generate_random_dag(nodes, p_edge_generation):
    G = nx.gnp_random_graph(nodes, p_edge_generation, directed=True)
    DAG = nx.DiGraph([(u, v) for (u, v) in G.edges() if u < v])
    if nx.is_directed_acyclic_graph(DAG):
        l = list(DAG.out_degree())
        print('Average out degree: ', mean([j[1] for j in l]))
        targets = [n for n, d in DAG.in_degree() if d == 0]
        if len(targets) > 1:
            new_node_id = max(DAG.nodes) + 1
            DAG.add_node(new_node_id)
            for u in targets:
                DAG.add_edge(new_node_id,u)
        return DAG
    print("I failed at generating DAG!!!")
    return None

def generate_random_hypergraph(nodes, out_degree, hyperedge_tail_size=1):
    dag = generate_random_dag(nodes, out_degree)
    if dag:
        drf = DRF()
        drf.load_dag(dag)
        if hyperedge_tail_size > 1:
            for sentence in drf.get_sentences():
                head = [sentence]
                predecessors = drf.get_hypergraph().get_predecessors(head)
                while predecessors.__len__() >= hyperedge_tail_size:
                    edges_to_join = random.sample(predecessors,hyperedge_tail_size)
                    tail = [drf.get_hypergraph().get_hyperedge_tail(p)[0] for p in edges_to_join]
                    drf.get_hypergraph().add_hyperedge(tail,head)
                    for e in edges_to_join:
                        drf.get_hypergraph().remove_hyperedge(e)
                        predecessors.remove(e)
        drf.hash_descendants()
        return drf
    return None

def draw_DAG(G):
    plt.figure()
    pos = nx.spring_layout(G)
    nx.draw_networkx_nodes(G, pos)
    nx.draw_networkx_edges(G, pos)
    plt.show()
    plt.title('DAG')
    plt.axis('off')
    plt.show()
