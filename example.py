"""Provides an example showing how to use the directed relational framework for collective decision making.
"""

from RelationalModel import *
from settings import *
import warnings
from matplotlib.cbook.deprecation import MatplotlibDeprecationWarning


__author__ = "Jordi Ganzer, Juan A. Rodríguez-Aguilar, Natalia Criado, Maite López-Sánchez, and Simon Parsons"
__copyright__ = "Copyright 2019"
__credits__ = ["Jordi Ganzer", "Juan A. Rodríguez-Aguilar", "Natalia Criado",
                    "Maite López-Sánchez", "Simon Parsons"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Juan A. Rodríguez-Aguilar"
__email__ = "jar@iiia.csic.es"
__status__ = "Development"

def run_hypergraph_example():

    # Example to illustrate how to handle a DRF with the halp library

    sentences = ['s_0', 's_1', 's_2', 's_3', 's_4', 's_5','s_6','s_7']
    relationships = [(['s_0'], ['s_1']),
                  (['s_0'], ['s_2']),
                  (['s_0'], ['s_3']),
                  (['s_0'], ['s_6']),
                  (['s_0'], ['s_7']),
                  (['s_1'], ['s_5']),
                  (['s_2', 's_3'], ['s_4']),
                  (['s_6', 's_7'], ['s_4']),
                  (['s_4'], ['s_5'])]
    targets = ['s_0']

    drf = DRF(sentences)
    drf.set_relationships(relationships)
    drf.set_targets(targets)
    drf.print_hypergraph()
    drf.write('hyper.txt')

    head = ['s_5']
    predecessors = drf.get_hypergraph().get_predecessors(head)
    print(predecessors)
    tail =[drf.get_hypergraph().get_hyperedge_tail(p)[0] for p in predecessors]
    print(tail)
    drf.get_hypergraph().add_hyperedge(tail, head)
    for p in predecessors:
        drf.get_hypergraph().remove_hyperedge(p)
    drf.print_hypergraph()


def run_article_example():
    # Example to illustrate the direct, indirect, and recursive aggregation functions
    # This example is the one in our journal paper

    # Build Directed Relational Framework by defining sentences and connections between them

    sentences = ['s_0', 's_1', 's_2', 's_3', 's_4', 's_5']
    # relationships = [(['s_0'], ['s_1']),
    #               (['s_0'], ['s_2']),
    #               (['s_0'], ['s_3']),
    #               (['s_2', 's_3'], ['s_4']),
    #               (['s_4'], ['s_5'])]
    relationships = [(['s_0'], ['s_1'],{'weight' : 2}),
                  (['s_0'], ['s_2'],{'weight' : 1}),
                  (['s_0'], ['s_3'],{'weight' : 1}),
                  (['s_2', 's_3'], ['s_4'],{'weight' : 1}),
                  (['s_4'], ['s_5'],{'weight' : 1})]
    targets = ['s_0']

    drf = DRF(sentences)
    drf.set_relationships(relationships)
    drf.set_targets(targets)
    drf.print_hypergraph()

    # Build opinions

    opinion1 = Opinion(drf, acceptance_domain, valuation_domain)
    opinion2 = Opinion(drf, acceptance_domain, valuation_domain)
    opinion3 = Opinion(drf, acceptance_domain, valuation_domain)

    opinion1.set_valuation('s_0', 0.9)
    opinion1.set_valuation('s_1', 0.0)
    opinion1.set_valuation('s_2', 0.7)
    opinion1.set_valuation('s_3', 1.0)
    opinion1.set_valuation('s_4', 1.0)
    opinion1.set_valuation('s_5', -1.0)

    opinion2.set_valuation('s_0', -0.5)
    opinion2.set_valuation('s_1', -1.0)
    opinion2.set_valuation('s_2', 1.0)
    opinion2.set_valuation('s_3', 0.5)
    opinion2.set_valuation('s_4', 1.0)
    opinion2.set_valuation('s_5', 1.0)

    opinion3.set_valuation('s_0', -0.5)
    opinion3.set_valuation('s_1', 0.0)
    opinion3.set_valuation('s_2', -0.8)
    opinion3.set_valuation('s_3', 0.5)
    opinion3.set_valuation('s_4', 1.0)
    opinion3.set_valuation('s_5', -1.0)

    opinion1.set_acceptance(['s_0'], ['s_1'], [0.2,0.5])
    opinion1.set_acceptance(['s_0'], ['s_2'], [0.1])
    opinion1.set_acceptance(['s_0'], ['s_3'], [1.0])
    opinion1.set_acceptance(['s_2', 's_3'], ['s_4'], [1.0])
    opinion1.set_acceptance(['s_4'], ['s_5'], [1.0])

    opinion2.set_acceptance(['s_0'], ['s_1'], [1.0,1.0])
    opinion2.set_acceptance(['s_0'], ['s_2'], [0.7])
    opinion2.set_acceptance(['s_0'], ['s_3'], [0.8])
    opinion2.set_acceptance(['s_2', 's_3'], ['s_4'], [1.0])
    opinion2.set_acceptance(['s_4'], ['s_5'], [0.5])

    opinion3.set_acceptance(['s_0'], ['s_1'], [0.6,0.2])
    opinion3.set_acceptance(['s_0'], ['s_2'], [1.0])
    opinion3.set_acceptance(['s_0'], ['s_3'], [1.0])
    opinion3.set_acceptance(['s_2', 's_3'], ['s_4'], [0.3])
    opinion3.set_acceptance(['s_4'], ['s_5'], [1.0])

    # Bundle opinions into profile
    profile = Profile(drf,acceptance_domain,valuation_domain)
    profile.add_opinion(opinion1)
    profile.add_opinion(opinion2)
    profile.add_opinion(opinion3)

    drf.hash_descendants()

    # Build the class that computes aggregation functions for the profile
    f = AggregationFunctions(drf, profile, acceptance_domain, valuation_domain)

    # Ignore warnings issued by networkx when plotting
    warnings.filterwarnings("ignore", category=MatplotlibDeprecationWarning)

    print('********************** AGGREGATION FUNCTIONS **********************\n')

    # Compute direct function over the profile and draw collective opinion

    print('DIRECT FUNCTION:')
    collective_opinion_direct = f.direct_function()
    print('Valuations: ', collective_opinion_direct.get_valuation_all())
    print('Acceptances: ', collective_opinion_direct.get_acceptance_all(), '\n')
    #
    # # #
    # Compute indirect function over the profile and draw collective opinion
    #
    print('INDIRECT FUNCTION:')
    collective_opinion_indirect = f.indirect_function()
    print('Valuations: ', collective_opinion_indirect.get_valuation_all())
    print('Acceptances: ', collective_opinion_indirect.get_acceptance_all(), '\n')
    #
    # #
    # Compute balanced function over the profile and draw collective opinion
    #
    print('BALANCED FUNCTION:')
    collective_opinion_balanced = f.balanced_function()
    print('Valuations: ', collective_opinion_balanced.get_valuation_all())
    print('Acceptances: ', collective_opinion_balanced.get_acceptance_all(), '\n')

    #
    # Compute recursive function over the profile and draw collective opinion
    #
    print('RECURSIVE FUNCTION:')
    collective_opinion_recursive = f.recursive_function()
    print('Valuations: ', collective_opinion_recursive.get_valuation_all())
    print('Acceptances: ', collective_opinion_recursive.get_acceptance_all())
    #


if __name__ == "__main__":
    run_article_example()