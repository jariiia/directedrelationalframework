**INTRODUCTION**

This distribution implements the Directed Relational Framework (DRF), a new  model to represent human debates and methods to obtain collective conclusions from them. 
A thorough description of the DRF model can be found in the following publication:


[1] A model to support collective reasoning: Formalization, analysis and computational assessment.
Jordi Ganzer-Ripoll, Natalia Criado, Maite López-Sánchez, Simon Parsons, Juan A. Rodríguez-Aguilar.
arXiv:2007.06850 [cs.AI], 2020. [https://arxiv.org/abs/2007.06850]


**REQUIREMENTS**

* *Python 3.7*
* *Halp*. The implementation of the DRF is based on the Hypergraphs Algorithmics Package halp
that provides both a directed and an undirected hypergraph implementation, as well as several
important and canonical algorithms that operate on these hypergraphs. URL: https://murali-group.github.io/halp/

**HOW TO START WITH THE DIRECTED RELATIONAL FRAMEWORK**

The **example.py** file reproduces the example employed in [1] to illustrate a simple example of a debate
as well as the result of the aggregated functions introduced in the paper.

The example shows:

* how to build a DRF
* how to build opinions 
* how to build an opinion profile
* how to run aggregation functions over an opinion profile


**HOW TO REPRODUCE THE EXPERIMENTS REPORTED IN [1]**

The **experiment.py** file allows to reproduce the experiments in the article above.
For that, it is necessary to conveniently set the parameters in the **experiment.cfg** confifuration file,
whose content is included below. There are four sections in the configuration file:

* *general*: to describe the number of problems to generate and run as well as the types of analysis to perform (participant, density, relationship size), and whether to generate plots or not.
* *participants*: to specify the number of sentences and participants as well as the avearge out degree in the DRF.
* *density*: to specify the number of sentences and participants as well as a list of avearge out degrees in the DRF. Specifying difference out degrees allows to compare DRFs with different densities regarding number of relationships.
* *relationships*: to specify the number of sentences and participants as well as different tail sizes, namely number of nodes (sentences) in the tail of relationships.





[general]

number_of_problems = 2

participant_analysis = yes

density_analysis = no

relationship_size_analysis = no

generate_graphs = yes


[participants]

number_of_sentences = 100,150,200

average_out_degree = 5.0

number_of_participants = 10000,30000, 50000

log_file = participants.csv

output_file = ParticipantSensitive.png


[density]

number_of_participants = 3000000

number_of_sentences = 100,150,200

average_out_degree = 2.0,5.0,10.0

log_file = density.csv

output_file = DensitySensitive.png


[relationships]

number_of_participants = 3000000

number_of_sentences = 100,150,200

average_out_degree = 5.0

tail_sizes = 1,2,3

log_file = relationships.csv

output_file = RelationshipSizeSensitive.png





